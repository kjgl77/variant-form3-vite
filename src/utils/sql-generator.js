import {isNotNull, traverseFieldWidgets} from "@/utils/util";

export function buildDefaultValueListFn(formConfig, widgetList, resultList) {
  return function(fieldWidget) {
    const fop = fieldWidget.options
    const fd = fop.defaultValue
    if (isNotNull(fd)) {
      if(fop.required){
      resultList.push(`${fop.name} VARCHAR(255) NOT NULL COMMENT '${fop.label.replace(/^(:|：)+|(:|：)+$/g, '')}',`)

      }else{
        resultList.push(`${fop.name} VARCHAR(255) COMMENT '${fop.label.replace(/^(:|：)+|(:|：)+$/g, '')}',`)
      }
    } else {
      if(fop.required){
        resultList.push(`${fop.name} VARCHAR(255) NOT NULL COMMENT '${fop.label.replace(/^(:|：)+|(:|：)+$/g, '')}',`)
  
        }else{
          resultList.push(`${fop.name} VARCHAR(255) COMMENT '${fop.label.replace(/^(:|：)+|(:|：)+$/g, '')}',`)
        }
    }
  }
}

// todo:模仿genVue2JS方法，生成sql
export const genSQL = function (formConfig, widgetList) {
  let defaultValueList = []
  traverseFieldWidgets(widgetList, (widget) => {
    buildDefaultValueListFn(formConfig, widgetList, defaultValueList)(widget)
  })

  const sqlTemplate =
` DROP TABLE IF EXISTS table_name;
  CREATE TABLE table_name(
    id INT NOT NULL AUTO_INCREMENT  COMMENT 'id' ,
    ${defaultValueList.join('\n')}
    PRIMARY KEY (id)
  )
    `
    return sqlTemplate
}