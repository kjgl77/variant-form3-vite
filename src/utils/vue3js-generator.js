import {
  buildDefaultValueListFn,
  buildRulesListFn
} from "@/utils/vue2js-generator";
import {traverseFieldWidgets,traverseContainerWidgets} from "@/utils/util";

function buildFieldOptionsFnV3(formConfig, widgetList, resultList) {
  return function(fieldWidget) {
    const fop = fieldWidget.options
    const ft = fieldWidget.type
    if ((ft === 'radio') || (ft === 'checkbox') || (ft === 'select') || (ft === 'cascader')) {
      resultList.push(`const ${fop.name}Options = ${JSON.stringify(fop.optionItems)}`)
    }
  }
}

export function buildUploadDataFnV3(formConfig, widgetList, resultList) {
  return function(fieldWidget) {
    const fop = fieldWidget.options
    const ft = fieldWidget.type
    if ((ft === 'picture-upload') || (ft === 'file-upload')) {
      resultList.push(`const ${fop.name}FileList = reactive([])`)
      resultList.push(`const ${fop.name}UploadHeaders = reactive({})`)
      resultList.push(`const ${fop.name}UploadData = reactive({})`)
    }
  }
}

export function buildActiveTabsV3(formConfig, widgetList) {
  let resultList = []
  const handlerFn = function (cw) {
    const cop = cw.options
    const ct = cw.type
    if (ct === 'tab') {
      cw.tabs.length > 0 && resultList.push(`const ${cop.name}ActiveTab = '${cw.tabs[0].options.name}'`)
    }
  }
  traverseContainerWidgets(widgetList, handlerFn)

  return resultList
}

export function buildCardFoldedV3(formConfig, widgetList) {
  let resultList = []
  const handlerFn = function (cw) {
    const cop = cw.options
    const ct = cw.type
    if (ct === 'card') {
      cop.showFold && resultList.push(`
      const ${cop.name}Folded = ref(false)
      const ${cop.name}ToggleCard = ()=>{
        ${cop.name}Folded.value = !${cop.name}Folded.value
      }
      `)
    }
  }
  traverseContainerWidgets(widgetList, handlerFn)

  return resultList
}

export const genVue3JS = function (formConfig, widgetList) {
  
  let defaultValueList = []
  let rulesList = []
  let fieldOptions = []
  let uploadData = []
  traverseFieldWidgets(widgetList, (widget) => {
    buildDefaultValueListFn(formConfig, widgetList, defaultValueList)(widget)
    buildRulesListFn(formConfig, widgetList, rulesList)(widget)
    buildFieldOptionsFnV3(formConfig, widgetList, fieldOptions)(widget)
    buildUploadDataFnV3(formConfig, widgetList, uploadData)(widget)
  })
  const activeTabs = buildActiveTabsV3(formConfig, widgetList)
  const cardFolded = buildCardFoldedV3(formConfig, widgetList)

  const v3JSTemplate =
`  import { reactive, ref } from 'vue'
  
      const ${formConfig.modelName} = reactive({
          ${defaultValueList.join('\n')}
        })
        
      const ${formConfig.rulesName} = reactive({
          ${rulesList.join('\n')}
        })
        
      ${activeTabs.join('\n')}

      ${cardFolded.join('\n')}
      
      ${fieldOptions.join('\n')}
        
      ${uploadData.join('\n')}
    
      const vForm = $ref(null)
      
      const submitForm = () => {
        vForm.validate(valid => {
          if (!valid) return
          
          //TODO: 提交表单
        })
      }
      
      const resetForm = () => {
        vForm.resetFields()
      }
      
`

  return v3JSTemplate
}

